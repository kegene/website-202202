export const NavMenuConfig = [
  {
    title: 'Dashboard',
    icon: 'el-icon-s-home',
    to: { name: 'Dashboard' }
  },
  {
    title: '報表',
    icon: 'el-icon-s-data',
    children: [
      {
        title: '出金'
      },
      {
        title: '入金'
      }
    ]
  },
  {
    title: '用戶中心',
    icon: 'el-icon-setting',
    children: [
      {
        title: '通知'
      },
      {
        title: '個人資料'
      }
    ]
  },
  {
    title: '轉帳',
    icon: 'el-icon-postcard'
  },
  {
    title: '設置',
    icon: 'el-icon-s-operation'
  },
  {
    title: '登出',
    icon: 'el-icon-switch-button',
    to: { name: 'Login' }
  }
]
