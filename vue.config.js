const publicPath = process.env.NODE_ENV === 'production'
  ? '/website-202202/'
  : process.env.NODE_ENV === 'test' ? '/dist'
    : '/'

module.exports = {
  publicPath
}
