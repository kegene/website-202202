# Element ui + Vue2 實作後台
`vue2` `element-ui` `axios` `scss`

![](./readme-assets/000.jpg)

## 簡介

element-ui 功能體驗。[DEMO | gitlab.io](https://kegene.gitlab.io/website-202202)

## 項目資訊

- Vue 2
- Element UI
- Vue-cli@4
- Axios

## 體驗功能

- 表格
- 表單
- 通知
- 欄位排版

## 感想

功能齊全，但是沒業務邏輯，實作起來內容空洞，感覺沒啥意義。